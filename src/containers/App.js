import React from "react";
import CompanyHeader from "../components/Company/Header";
import EmployeeGrid from "../components/Employee/Grid";

import "normalize.css";
import styles from "./App.module.css";

const App = () => {
  return (
    <div className={styles.app}>
      <CompanyHeader />
      <EmployeeGrid />
    </div>
  );
};

export default App;
