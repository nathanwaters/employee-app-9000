import { useState, useEffect } from "react";

export const useFetch = (url, id) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    fetch(url)
      .then(res => {
        if (!res.ok) {
          throw new Error("Failed to fetch.");
        }
        return res.json();
      })
      .then(data => {
        setLoading(false);
        //return specific employee data
        if (id) {
          data = data.employees.filter(x => x.id === id)[0];
        }
        setData(data);
      })
      .catch(err => {
        console.log(err);
        setLoading(false);
      });
  }, [url, id]);

  return { data, loading };
};
