import React from "react";
import { useFetch } from "../../utils/fetchUtil";

import styles from "./Header.module.css";

const CompanyHeader = () => {
  const { data, loading } = useFetch("company.json");
  const company = data ? data.companyInfo : {};

  let content = "Loading...";
  if (!loading) {
    content = (
      <div className={styles.header}>
        <div>
          <h1 className={styles.title}>{company.companyName}</h1>
          <h4 className={styles.motto}>{company.companyMotto}</h4>
        </div>
        <div>
          <p className={styles.est}>
            Since {new Date(company.companyEst).getFullYear()}
          </p>
        </div>
      </div>
    );
  }

  return content;
};

export default CompanyHeader;
