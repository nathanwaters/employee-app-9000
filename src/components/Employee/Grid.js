import React, { useState } from "react";
import { useFetch } from "../../utils/fetchUtil";
import { dynamicSort } from "../../utils/filterUtil";
import EmployeeFilter from "./Filter";
import EmployeeCard from "./Card";
import EmployeeModal from "./Modal";
import styles from "./Grid.module.css";

const EmployeeGrid = () => {
  const [filter, setFilter] = useState({
    sort: "firstName",
    direction: "",
    search: ""
  });
  const [selectedEmployee, setSelectedEmployee] = useState(null);
  const [showModal, setShowModal] = useState(false);

  const { data, loading } = useFetch("employees.json");
  let employees = data ? data.employees : [];

  //filter employee array by search + sort
  let filteredEmployees = employees
    .filter(
      x =>
        (x.firstName + " " + x.lastName)
          .toUpperCase()
          .indexOf(filter.search.toUpperCase()) !== -1
    )
    .sort(dynamicSort(filter.direction + filter.sort));

  //filters have changed
  const filterHandler = (field, value) => {
    setFilter({
      ...filter,
      [field]: value
    });
  };

  //employee card selected
  const selectEmployeeHandler = id => {
    setSelectedEmployee(id);
    setShowModal(true);
  };

  let content = "Loading...";
  if (!loading) {
    content = (
      <div className={styles.wrapper}>
        <EmployeeFilter onFilter={filterHandler} />
        <div className={styles.grid}>
          {filteredEmployees.length > 0 ? (
            filteredEmployees.map(employee => (
              <EmployeeCard
                key={employee.id}
                id={employee.id}
                avatar={employee.avatar}
                name={employee.firstName + " " + employee.lastName}
                jobTitle={employee.jobTitle}
                onSelectEmployee={selectEmployeeHandler}
              />
            ))
          ) : (
            <p>No results</p>
          )}
        </div>
        {showModal && (
          <EmployeeModal
            selectedEmployee={selectedEmployee}
            onCloseModal={() => setShowModal(false)}
          />
        )}
      </div>
    );
  }

  return content;
};

export default EmployeeGrid;
