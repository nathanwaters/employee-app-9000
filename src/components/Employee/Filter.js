import React from "react";
import PropTypes from "prop-types";
import styles from "./Filter.module.css";

const EmployeeFilter = props => {
  let content = (
    <div className={styles.employeesHeader}>
      <h2>Our Employees</h2>
      <div className={styles.filters}>
        <p className={styles.filterText}>Sort by</p>
        <select
          className={styles.filterOpt}
          onChange={e => props.onFilter("sort", e.target.value)}
        >
          <option value="firstName">First Name</option>
          <option value="lastName">Last Name</option>
          <option value="dateJoined">Date Joined</option>
          <option value="age">Age</option>
        </select>
        <select
          className={styles.filterOpt}
          onChange={e => props.onFilter("direction", e.target.value)}
        >
          <option value="">Asc</option>
          <option value="-">Desc</option>
        </select>
        <input
          className={styles.filterOpt}
          onChange={e => props.onFilter("search", e.target.value)}
          type="text"
          placeholder="Search names..."
        />
      </div>
    </div>
  );

  return content;
};

EmployeeFilter.propTypes = {
  onFilter: PropTypes.func
};

export default EmployeeFilter;
