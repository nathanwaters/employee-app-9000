import React from "react";
import PropTypes from "prop-types";
import styles from "./Card.module.css";

const EmployeeCard = props => (
  <div className={styles.card} onClick={() => props.onSelectEmployee(props.id)}>
    <div className={styles.image}>
      <img src={props.avatar} alt={props.name} />
    </div>
    <div className={styles.details}>
      <h4 className={styles.name}>{props.name}</h4>
      <p className={styles.jobTitle}>{props.jobTitle}</p>
    </div>
  </div>
);

EmployeeCard.propTypes = {
  id: PropTypes.string,
  avatar: PropTypes.string,
  name: PropTypes.string,
  jobTitle: PropTypes.string
};

export default EmployeeCard;
