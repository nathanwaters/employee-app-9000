import React, { useRef } from "react";
import PropTypes from "prop-types";
import { useFetch } from "../../utils/fetchUtil";
import styles from "./Modal.module.css";
import { IoMdClose } from "react-icons/io";

const EmployeeModal = props => {
  const modalRef = useRef();

  //fetch specific employee data
  const { data, loading } = useFetch("employees.json", props.selectedEmployee);
  const employee = data ? data : [];

  //make some pretty variables
  const fullName = employee.firstName + " " + employee.lastName;
  const dateJoined = new Date(employee.dateJoined).toLocaleDateString([], {
    month: "short",
    year: "numeric"
  });

  //modal has been clicked
  const modalClickHandler = e => {
    if (modalRef.current.contains(e.target)) return; //clicked inside modal
    props.onCloseModal();
  };

  let content = "Loading...";
  if (!loading) {
    content = (
      <div className={styles.modal} onClick={modalClickHandler}>
        <div className={styles.modalInner} ref={modalRef}>
          <button className={styles.close} onClick={props.onCloseModal}>
            <IoMdClose />
          </button>
          <div className={styles.section}>
            <div className={styles.left}>
              <img
                className={styles.profileImg}
                src={employee.avatar}
                alt={fullName}
              />
            </div>
            <div className={styles.right}>
              <h1>{fullName}</h1>
            </div>
          </div>
          <div className={styles.section}>
            <div className={styles.left}>
              <h4>{employee.jobTitle}</h4>
              <p>Age: {employee.age}</p>
              <p>Joined: {dateJoined}</p>
            </div>
            <div className={styles.right}>
              <p>{employee.bio}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return content;
};

EmployeeModal.propTypes = {
  selectedEmployee: PropTypes.string,
  onCloseModal: PropTypes.func
};

export default EmployeeModal;
